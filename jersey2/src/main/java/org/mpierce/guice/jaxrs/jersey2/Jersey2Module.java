package org.mpierce.guice.jaxrs.jersey2;

import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Scopes;
import com.google.inject.servlet.ServletModule;
import org.glassfish.jersey.servlet.ServletContainer;
import org.mpierce.guice.jaxrs.common.SampleResourceModule;

public class Jersey2Module extends AbstractModule {

    @Override
    protected void configure() {
        install(new SampleResourceModule());

        bind(ObjectMapperContextResolver.class);
        bind(ServletContainer.class).toProvider(ServletContainerProvider.class).in(Scopes.SINGLETON);
        bind(Jersey2App.class);

        install(new ServletModule() {
            @Override
            protected void configureServlets() {
                serve("/*").with(ServletContainer.class);
            }
        });
    }

    static class ServletContainerProvider implements com.google.inject.Provider<ServletContainer> {

        private final Jersey2App app;

        @Inject
        ServletContainerProvider(Jersey2App app) {
            this.app = app;
        }

        @Override
        public ServletContainer get() {
            return new ServletContainer(app);
        }
    }
}
