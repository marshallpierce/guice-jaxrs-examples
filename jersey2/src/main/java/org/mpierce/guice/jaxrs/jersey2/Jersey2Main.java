package org.mpierce.guice.jaxrs.jersey2;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.palominolabs.http.server.HttpServerConnectorConfig;
import com.palominolabs.http.server.HttpServerWrapperConfig;
import com.palominolabs.http.server.HttpServerWrapperFactory;
import com.palominolabs.http.server.HttpServerWrapperModule;
import com.squarespace.jersey2.guice.JerseyGuiceUtils;
import java.util.logging.LogManager;
import org.mpierce.guice.jaxrs.common.JacksonModule;
import org.slf4j.bridge.SLF4JBridgeHandler;

class Jersey2Main {

    private static final String HOST = "localhost";
    private static final int PORT = 8080;

    public static void main(String[] args) throws Exception {
        LogManager.getLogManager().reset();
        SLF4JBridgeHandler.install();

        // start up an http server
        HttpServerWrapperConfig config = new HttpServerWrapperConfig()
                .withHttpServerConnectorConfig(HttpServerConnectorConfig.forHttp(HOST, PORT));

        Injector injector = Guice.createInjector(new ServiceModule());

        JerseyGuiceUtils.install(new GuiceServiceLocatorGenerator(injector));

        injector.getInstance(HttpServerWrapperFactory.class)
                .getHttpServerWrapper(config)
                .start();
    }

    private static class ServiceModule extends AbstractModule {

        @Override
        protected void configure() {
            binder().requireExplicitBindings();

            install(new HttpServerWrapperModule());
            install(new JacksonModule());

            install(new Jersey2Module());
        }
    }
}
