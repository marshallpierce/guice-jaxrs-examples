Examples of using Guice together with various JAX-RS providers.

- `jersey2` uses [jersey2-guice](https://github.com/Squarespace/jersey2-guice).
- `resteasy` uses [Resteasy's built in integration](http://docs.jboss.org/resteasy/docs/3.0.1.Final/userguide/html/Guice1.html).

[guice-cxf](https://code.google.com/p/guice-cxf/) only knows how to make a `JAXRSServerFactoryBean`, not integrate with an existing server, and I don't like that, so CXF doesn't get to play.

[Restlet](http://restlet.com/) didn't seem to have much to offer in the way of Guice integration for its JAX-RS layer.

Jersey 1 has Guice support but it's long since EOL'd.

Try any of 

- `./gradlew :jersey2:run`
- `./gradlew :resteasy:run`

and hit http://localhost:8080/resource/subresource. See [`SampleResource`](https://bitbucket.org/marshallpierce/guice-jaxrs-examples/src/master/common/src/main/java/org/mpierce/guice/jaxrs/common/SampleResource.java?at=master) for what produces that.

You can also hit http://localhost:8080/resource/now to see customizing the ObjectMapper used by Jackson in action.
