package org.mpierce.guice.jaxrs.resteasy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.google.inject.Singleton;
import com.google.inject.servlet.ServletModule;
import com.palominolabs.http.server.BinderProviderCapture;
import com.palominolabs.http.server.HttpServerConnectorConfig;
import com.palominolabs.http.server.HttpServerWrapperConfig;
import com.palominolabs.http.server.HttpServerWrapperFactory;
import com.palominolabs.http.server.HttpServerWrapperModule;
import org.jboss.resteasy.plugins.guice.GuiceResteasyBootstrapServletContextListener;
import org.jboss.resteasy.plugins.server.servlet.HttpServletDispatcher;
import org.mpierce.guice.jaxrs.common.JacksonModule;
import org.mpierce.guice.jaxrs.common.SampleResourceModule;
import org.slf4j.bridge.SLF4JBridgeHandler;

import javax.servlet.ServletContextListener;
import java.util.logging.LogManager;

class ResteasyMain {

    private static final String HOST = "localhost";
    private static final int PORT = 8080;

    public static void main(String[] args) throws Exception {

        LogManager.getLogManager().reset();
        SLF4JBridgeHandler.install();

        // start up an http server
        HttpServerWrapperConfig config = new HttpServerWrapperConfig()
            .withHttpServerConnectorConfig(HttpServerConnectorConfig.forHttp(HOST, PORT));

        BinderProviderCapture<? extends ServletContextListener> listenerProvider =
            new BinderProviderCapture<>(GuiceResteasyBootstrapServletContextListener.class);

        config.addServletContextListenerProvider(listenerProvider);

        Injector injector = Guice.createInjector(new ServiceModule(listenerProvider));

        injector.getInstance(HttpServerWrapperFactory.class)
            .getHttpServerWrapper(config)
            .start();
    }

    private static class ServiceModule extends AbstractModule {

        private final BinderProviderCapture<?> listenerProvider;

        public ServiceModule(BinderProviderCapture<?> listenerProvider) {
            this.listenerProvider = listenerProvider;
        }

        @Override
        protected void configure() {
            binder().requireExplicitBindings();

            install(new HttpServerWrapperModule());
            install(new JacksonModule());
            install(new SampleResourceModule());

            bind(GuiceResteasyBootstrapServletContextListener.class);

            install(new ServletModule() {
                @Override
                protected void configureServlets() {
                    bind(HttpServletDispatcher.class).in(Scopes.SINGLETON);
                    serve("/*").with(HttpServletDispatcher.class);
                }
            });

            listenerProvider.saveProvider(binder());
        }

        @Provides
        @Singleton
        JacksonJsonProvider getJacksonJsonProvider(ObjectMapper objectMapper) {
            return new JacksonJsonProvider(objectMapper);
        }
    }
}
