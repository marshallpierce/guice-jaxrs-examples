package org.mpierce.guice.jaxrs.common;

import com.google.inject.AbstractModule;

public final class SampleResourceModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(RandomSource.class);
        bind(SampleResource.class);
    }
}
