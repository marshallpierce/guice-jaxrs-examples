package org.mpierce.guice.jaxrs.common;

final class RandomSource {

    boolean flipCoin() {
        // it's an unfair coin
        return Math.random() > 0.9;
    }
}
