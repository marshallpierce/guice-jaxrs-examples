package org.mpierce.guice.jaxrs.common;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.OffsetDateTime;

final class JsonPojo {
    private final OffsetDateTime now = OffsetDateTime.now();

    @JsonProperty
    public OffsetDateTime getNow() {
        return now;
    }
}
